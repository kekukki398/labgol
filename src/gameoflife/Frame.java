package gameoflife;

import java.util.Arrays;

public class Frame {

	public final static String ALIVE = "X";
	public final static String DEAD = "-";
	private int width;
	private int height;
	private String[] frame = new String[] {};

	public Frame(int width, int height) {
		this.width = width;
		this.height = height;
		makeEmptyRows();
		addDeadColumnCells();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int counter = 0;
		for (String frame : frame) {
			if (counter == height - 1) {
				sb.append(frame);
			} else {
				sb.append(frame).append('\n');
			}
			counter++;
		}
		return sb.toString();
	}

	private void makeEmptyRows() {
		frame = new String[height];
		Arrays.fill(frame, "");
	}

	private void addDeadColumnCells() {
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				frame[row] += Frame.DEAD;
			}
		}
	}

	@Override
	public boolean equals(Object framObj) {
		Frame frame = (Frame) framObj;
		if (Arrays.deepEquals(this.frame, frame.frame))
			return true;
		return false;
	}

	public Integer getNeighbourCount(int x, int y) {
		int cellCount = 0;
		if (x == width) {
			cellCount = incrementIfTopCell(x, y, cellCount);
			cellCount = incrementIfTopLeftCell(x, y, cellCount);
			cellCount = incrementIfLeftCell(x, y, cellCount);
			if (y == height) {
				return cellCount;
			} else {
				cellCount = incrementIfBottomLeftCell(x, y, cellCount);
				cellCount = incrementIfBottomCell(x, y, cellCount);
			}
			return cellCount;
		}
		if (y == height) {
			cellCount = incrementIfTopCell(x, y, cellCount);
			cellCount = incrementIfTopLeftCell(x, y, cellCount);
			cellCount = incrementIfLeftCell(x, y, cellCount);
			if (x == width) {
				return cellCount;
			} else {
				cellCount = incrementIfTopRightCell(x, y, cellCount);
				cellCount = incrementIfRightCell(x, y, cellCount);
			}
			return cellCount;
		}
		cellCount = checkAllEightNeighbours(x, y, cellCount);
		return cellCount;
	}

	private int checkAllEightNeighbours(int x, int y, int cellCount) {
		cellCount = incrementIfTopLeftCell(x, y, cellCount);
		cellCount = incrementIfTopCell(x, y, cellCount);
		cellCount = incrementIfTopRightCell(x, y, cellCount);
		cellCount = incrementIfLeftCell(x, y, cellCount);
		cellCount = incrementIfRightCell(x, y, cellCount);
		cellCount = incrementIfBottomLeftCell(x, y, cellCount);
		cellCount = incrementIfBottomCell(x, y, cellCount);
		cellCount = incrementIfBottomRightCell(x, y, cellCount);
		return cellCount;
	}

	private int incrementIfBottomRightCell(int x, int y, int cellCount) {
		if (isAlive(x + 1, y + 1)) {
			cellCount++;
		}
		return cellCount;
	}

	private int incrementIfRightCell(int x, int y, int cellCount) {
		if (isAlive(x + 1, y)) {
			cellCount++;
		}
		return cellCount;
	}

	private int incrementIfTopRightCell(int x, int y, int cellCount) {
		if (isAlive(x + 1, y - 1)) {
			cellCount++;
		}
		return cellCount;
	}

	private int incrementIfBottomCell(int x, int y, int cellCount) {
		if (isAlive(x, y + 1)) {
			cellCount++;
		}
		return cellCount;
	}

	private int incrementIfBottomLeftCell(int x, int y, int cellCount) {
		if (isAlive(x - 1, y + 1)) {
			cellCount++;
		}
		return cellCount;
	}

	private int incrementIfLeftCell(int x, int y, int cellCount) {
		if (isAlive(x - 1, y)) {
			cellCount++;
		}
		return cellCount;
	}

	private int incrementIfTopLeftCell(int x, int y, int cellCount) {
		if (isAlive(x - 1, y - 1)) {
			cellCount++;
		}
		return cellCount;
	}

	private int incrementIfTopCell(int x, int y, int cellCount) {
		if (isAlive(x, y - 1)) {
			cellCount++;
		}
		return cellCount;
	}

	public boolean isAlive(int x, int y) {
		x -= 1;
		y -= 1;
		for (int row = 0; row <= height + 1; row++) {
			for (int col = 0; col <= width + 1; col++) {
				if (col == x && row == y) {
					if (String.valueOf(frame[row].charAt(col)).equals(Frame.ALIVE)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public void markAlive(int x, int y) {
		x -= 1;
		y -= 1;
		for (int row = 0; row <= height + 1; row++) {
			for (int col = 0; col <= width + 1; col++) {
				if (col == x && row == y) {
					StringBuilder sb = new StringBuilder(frame[row]);
					sb.setCharAt(col, Frame.ALIVE.charAt(0));
					frame[row] = sb.toString();
				}
			}
		}
	}

	public void markDead(int x, int y) {
		x -= 1;
		y -= 1;
		for (int row = 0; row <= height + 1; row++) {
			for (int col = 0; col <= width + 1; col++) {
				if (col == x && row == y) {
					StringBuilder sb = new StringBuilder(frame[row]);
					sb.setCharAt(col, Frame.DEAD.charAt(0));
					frame[row] = sb.toString();
				}
			}
		}
	}

	public void deadCellNextFrameStatus(Frame frame, int x, int y) {
		if (getNeighbourCount(x, y) == 3) {
			frame.markAlive(x, y);
			String.valueOf(this.frame[y - 1].charAt(x - 1)).replace("-", "X");
		}
	}

	public void aliveCellNextFrameStatus(Frame frame, int x, int y) {
		if (getNeighbourCount(x, y) < 2) {
			frame.markDead(x, y);
			String.valueOf(this.frame[y - 1].charAt(x - 1)).replace("X", "-");
		} else if (getNeighbourCount(x, y) == 2 || getNeighbourCount(x, y) == 3) {
			frame.markAlive(x, y);
		} else {
			frame.markDead(x, y);
			String.valueOf(this.frame[y - 1].charAt(x - 1)).replace("X", "-");
		}
	}

	public Frame nextFrame() {

		Frame frame = new Frame(width, height);

		for (int row = 1; row < height + 1; row++) {
			for (int col = 1; col < width + 1; col++) {
				if (isAlive(col, row)) {
					aliveCellNextFrameStatus(frame, col, row);
				} else {
					deadCellNextFrameStatus(frame, col, row);
				}
			}
		}
		return frame;
	}

}