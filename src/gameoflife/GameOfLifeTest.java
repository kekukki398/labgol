package gameoflife;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class GameOfLifeTest {
	
	@Test
	public void markAliveMethodWorksCorrectly(){
		Frame frame = new Frame(2, 2);
		assertThat(frame.isAlive(2, 2), is(false));
		
		frame.markAlive(2, 2);
		assertThat(frame.isAlive(2, 2), is(true));
	}
	
	@Test
	public void markDeadMethodWorksCorrectly(){
		Frame frame = new Frame(2,2);
		frame.markDead(2, 2);
		assertThat(frame.isAlive(2, 2), is(false));
	}
	
	@Test
    public void isAliveMethodWorksCorrectly(){
		Frame frame = new Frame(2,1);
		frame.markAlive(2, 1);
        assertThat(frame.isAlive(2, 1), is(true));
        assertThat(frame.isAlive(1, 1), is(false));
    }
	
	@Test
    public void overriddenEqualsMethodWorksCorrectly() {
        Frame frame = new Frame(3, 3);
        frame.markAlive(1, 1);
        frame.markAlive(2, 2);
        frame.markAlive(3, 3);
        Frame frame2 = new Frame(3, 3);
        frame2.markAlive(1, 1);
        frame2.markAlive(2, 2);
        frame2.markAlive(3, 3);

        assertThat(frame, equalTo(frame2));
        assertThat(frame.equals(frame2), is(true));
    }
	
	@Test
	public void getFrameReadsInputCorrectly(){
		Frame frame = new Frame(3, 2);
		frame.markAlive(1, 1);
		frame.markAlive(3, 1);
		frame.markAlive(2, 2);
		
		Frame frame2 = getFrame("X-X",
							    "-X-");
		
		Frame frame3 = getFrame("x-x",
			    				"-x-");

		assertThat(frame.equals(frame2), is(true));
		assertThat(frame.equals(frame3), is(false));
	}
	
	@Test
    public void overriddenToStringMethodWorksCorrectly(){
		Frame frame = getFrame("------",
				               "--XX--",
				               "-X--X-",
				               "--XX--",
				               "------",
				               "--X--X");
		
        String expected = "------\n"
				        + "--XX--\n"
				        + "-X--X-\n"
				        + "--XX--\n"
				        + "------\n"
				        + "--X--X";

        assertThat(frame.toString(), is(expected));
    }
	
	@Test
    public void aliveCellNeighboursAreCountedCorrectly() {
        Frame frame = getFrame("XXX",
                               "X-X",
                               "XXX");
        
        assertThat(frame.getNeighbourCount(2, 2), is(8));
    }
	
	@Test
	public void middleRightNeighboursAreCountedCorrectly() {
		Frame frame = getFrame("XX",
							   "X-",
							   "XX");
		
		assertThat(frame.getNeighbourCount(2, 2), is(5));
	}
	
	@Test
	public void bottomRightNeighboursAreCountedCorrectly() {
		Frame frame = getFrame("XX",
							   "X-");
		
		assertThat(frame.getNeighbourCount(2, 2), is(3));
	}
	
	@Test
	public void bottomMiddleNeighboursAreCountedCorrectly() {
		Frame frame = getFrame("XXX",
							   "X-X");
		
		assertThat(frame.getNeighbourCount(2, 2), is(5));
	}
	
	@Test
	public void bottomLeftNeighboursAreCountedCorrectly() {
		Frame frame = getFrame("XX",
							   "-X");
		
		assertThat(frame.getNeighbourCount(1, 2), is(3));
	}
	
	@Test
	public void aliveCellHasLessThanTwoAliveNeighbours() {
		Frame frame = getFrame("X-",
							   "--");

		Frame expected = getFrame("--",
				   				  "--");
		frame.aliveCellNextFrameStatus(frame, 1, 1);
		assertTrue(frame.equals(expected));
	}
	
	@Test
	public void aliveCellHasTwoAliveNeighbours() {
		Frame frame = getFrame("X-",
							   "XX");
		Frame expected = getFrame("X-",
 				  				  "XX");
		frame.aliveCellNextFrameStatus(frame, 1, 1);
		assertTrue(frame.equals(expected));
	}
	
	@Test
	public void aliveCellHasThreeAliveNeighbours() {
		Frame frame = getFrame("XX",
				   			   "XX");
		Frame expected = getFrame("XX",
			  				      "XX");
		frame.aliveCellNextFrameStatus(frame, 1, 1);
		assertTrue(frame.equals(expected));
	}
	
	@Test
	public void aliveCellHasThreeOrMoreNeighbours() {
		Frame frame = getFrame("XXX",
							   "XX-");
		Frame expected = getFrame("XXX",
						      	  "X--");
		frame.aliveCellNextFrameStatus(frame, 2, 2);
		assertTrue(frame.equals(expected));
	}
	
	@Test
	public void deadCellHasExactlyThreeNeighbours() {
		Frame frame = getFrame("X-X",
				   			   "-X-");
		Frame expected = getFrame("XXX",
					      	  	  "-X-");
		frame.deadCellNextFrameStatus(frame, 2, 1);
		assertTrue(frame.equals(expected));
	}
	
    @Test
    public void stillWorksCorrectly() {
        Frame frame = getFrame("------",
                               "--XX--",
                               "-X--X-",
                               "--XX--",
                               "------");

        assertThat(frame.nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void pulsarWorksCorrectly() {
        Frame frame = getFrame("------",
                               "-XX---",
                               "-X----",
                               "----X-",
                               "---XX-",
                               "------");

        Frame expected = getFrame("------",
                                  "-XX---",
                                  "-XX---",
                                  "---XX-",
                                  "---XX-",
                                  "------");

        assertThat(frame.nextFrame(), is(equalTo(expected)));
        assertThat(frame.nextFrame().nextFrame(), is(equalTo(frame)));
    }

    @Test
    public void gliderWorksCorrectly() {
        Frame frame1 = getFrame("-X----",
                                "--XX--",
                                "-XX---",
                                "------");

        Frame frame2 = getFrame("--X---",
                                "---X--",
                                "-XXX--",
                                "------");

        Frame frame3 = getFrame("------",
                                "-X-X--",
                                "--XX--",
                                "--X---");

        assertThat(frame1.nextFrame(), is(equalTo(frame2)));
        assertThat(frame2.nextFrame(), is(equalTo(frame3)));
    }

    private Frame getFrame(String ... rows) {	
    	
    	Frame frame = new Frame(rows[0].length(), rows.length);
    	
    	for (int row = 0; row < rows.length; row++) {
			for (int col = 0; col < rows[row].length(); col++) {
				if (String.valueOf(rows[row].charAt(col)).equals(Frame.ALIVE)){
					frame.markAlive(col + 1, row + 1);
				}
			}
		}	
    	
        return frame;
    }
}